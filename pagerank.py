#This code written by Ibrahim Alyami completed on April 17,2019 For COMP460
class Graph(object):# The object here is the graph that we created(We pass it to the rest of methods)
	def __init__(self, graph_dict=None):# Constructor class and using self here is help us access the arributes and methods here
		if graph_dict == None:
			graph_dict = {}
		self.graph_dict = graph_dict
	
	def vertices(self): #This class to print the vertices of the graph
		print(list(self.graph_dict.keys()))
	
	
	
	def edges(self): #This class to print the deges of the class by calling generate_edges method
		print("")
		print(self.generate_edges())


	def generate_edges(self):
		
		edges = []
		for node in self.graph_dict:
			for neighbour in self.graph_dict[node]:
				edges.append((node, neighbour))
		return edges

	

	def finder(self,node):#Check if values does exist or not
		for k in self.graph_dict.keys():# Apply the same concept from node A for all nodes
			if node in k :
				print("Yes, the node is in the graph ")
				break
			else:
				print("No, the node is not in the graph ")
				break
	


	def counting_keys(self):# Count how many nodes in the graph
		print("")
		print("How many nodes:"+str(len(self.graph_dict.keys())))
	

	

	def get_nodes_outgoinglinks(self):#To print nodes and thier outgoing links or(values)
		for k, v in self.graph_dict.items():
			print("")
			print(k + ' is going to these pages' + str(v))



	def counting_values(self):# Count how many nodes for each node(i.e for page A it has 3 outgoing links)
		global AllOutgoingLinks # To uses it in other class
		AllOutgoingLinks = []
		for k, v in self.graph_dict.items():
			vals = len(v)
			AllOutgoingLinks.append(vals)
		print("")
		print("Number of outgoing links for the Pages are "+str(AllOutgoingLinks.copy()))
		print("")
		

	

	def individual_pagerank_calculation(self,node):# Count the PR for a single page
		pageranklist = [1]# Created a list where the first iteration on page rank is by default one
		data = pageranklist[0]
		y = []
		print("")
		print("The first pagerank for this page is :"+str(data))
		print("")

		for k, v in self.graph_dict.items():
			if node in v :
				outgoinglinks = len(v)
				x = data/outgoinglinks
				y.append(x)
				y_sum=sum(y)
				pageranklist.append(y_sum)
				#print("A copy of the list"+str(pageranklist.copy()))
		print("The new PR is for node " + node + " is " +str(pageranklist[-1]))
		print("")
		print("----------------------THIS IS PR WITHOUT DAMPING---------------------------------")
		print("")

	def pageranks_calculation(self,nodeA,nodeB,nodeC,nodeD,nodeE,nodeF):#Calculate PR Wihout Damping for all nodes
		PageRanklist = [1,1,1,1,1,1]# Created a list where the first iteration on page rank is by default one
		a = [] #  list for node A
		b = [] #  list for node B
		c = [] #  list for node C
		d = [] #  list for node D
		e = [] #  list for node E
		f = [] #  list for node F
		allPR = [] # The final list for the graph PR
		print("")
		print("The first pagerank for these pages is :"+str(PageRanklist))
		print("")
		NumberOfIterations= int(input("Enter the number of Iterations: "))
		print("")
		
		while NumberOfIterations>0:# This loop to define how many iterations we wish to do
			print("---The next PageRank for all pages are :")
			print("")
			for k, v in self.graph_dict.items():  # Loop to find the PR for node A(We need a loop to go over the nodes that has incoming Links to A which here is 3 nodes (E,D,F)
				if nodeA in v :
					outgoinglinks = len(v)
					x = PageRanklist[3]/AllOutgoingLinks[3]+PageRanklist[4]/AllOutgoingLinks[4]+PageRanklist[5]/AllOutgoingLinks[5]
					a.append(x)
					PageRanklist.append(a[-1])
			print("The new PR for node  " + nodeA + " is " +str(PageRanklist[-1]))#Print the last value from the list
			allPR.append(PageRanklist[-1])# Append the final value from PageRanklist to the new list
			del a[:]# Cleaning the List from unneccssary values
			a.append(allPR[-1])
			del PageRanklist[6:9]# Cleaning the List from unneccssary values
			PageRanklist[0]=a[0]
			
			
			
			for k, v in self.graph_dict.items():# Apply the same concept from node A for all nodes
				if nodeB in v :
					outgoinglinks = len(v)
					x = PageRanklist[0]/AllOutgoingLinks[0]+PageRanklist[5]/AllOutgoingLinks[5]
					b.append(x)
					PageRanklist.append(b[-1])
			print("The new PR for node  " + nodeB + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del b[:]
			b.append(allPR[-1])
			del PageRanklist[6:9]
			PageRanklist[1]=b[0]
			
			for k, v in self.graph_dict.items():# Apply the same concept from node A for all nodes
				if nodeC in v :
					outgoinglinks = len(v)
					x = PageRanklist[0]/AllOutgoingLinks[0]+PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[3]/AllOutgoingLinks[3]
					c.append(x)
					PageRanklist.append(c[-1])
			print("The new PR for node  " + nodeC + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del c[:]
			c.append(allPR[-1])
			del PageRanklist[6:9]
			PageRanklist[2]=c[0]
			
			for k, v in self.graph_dict.items():# Apply the same concept from node A for all nodes
				if nodeD in v :
					outgoinglinks = len(v)
					x = PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[2]/AllOutgoingLinks[2]
					d.append(x)
					PageRanklist.append(d[-1])
			print("The new PR for node  " + nodeD + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del d[:]
			d.append(allPR[-1])
			del PageRanklist[6:9]
			PageRanklist[3]=d[0]

			for k, v in self.graph_dict.items():# Apply the same concept from node A for all nodes
				if nodeE in v :
					outgoinglinks = len(v)
					x = PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[2]/AllOutgoingLinks[2]+PageRanklist[3]/AllOutgoingLinks[3]+PageRanklist[5]/AllOutgoingLinks[5]
					e.append(x)
					PageRanklist.append(e[-1])
			print("The new PR for node  " + nodeE + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del e[:]
			e.append(allPR[-1])
			del PageRanklist[6:10]
			PageRanklist[4]=e[0]

			for k, v in self.graph_dict.items():# Apply the same concept from node A for all nodes
				if nodeF in v :
					outgoinglinks = len(v)
					x = PageRanklist[0]/AllOutgoingLinks[0]+PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[3]/AllOutgoingLinks[3]
					f.append(x)
					PageRanklist.append(f[-1])
			print("The new PR for node  " + nodeF + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del f[:]
			f.append(allPR[-1])
			del PageRanklist[6:10]
			PageRanklist[5]=f[0]
			
			print("")
			print("")
			NumberOfIterations = NumberOfIterations-1#Decrement the loop
		
		print("")
		print("----------------------THIS IS PR WITH DAMPING---------------------------------")
		print("")

	def pagerank_withDamping(self,nodeA,nodeB,nodeC,nodeD,nodeE,nodeF): # Calculate PR with Damping
			
		PageRanklist = [1,1,1,1,1,1]# Created a list where the first iteration on page rank is by default one
		a = []
		b = []
		c = []
		d = []
		e = []
		f = []
		allPR = []
		print("")
		print("The first pagerank with damping for these pages is :"+str(PageRanklist))
		print("")
		DampingFactor= float(input("Please Enter the Dapming Factor:"))
		print("")
		NumberOfIterations= int(input("Enter the number of Iterations: "))
		print("")
		PRWithDamping_List1 =[]#Old list that we need only some values then we going to append some values to the following list(Final list)(look at line 220 & 221
		PRWithDamping_List2=[]
		while NumberOfIterations>0:#This loop to define how many iterations we wish to do
			print("---The next PageRank With Damping Factor for all pages are :")
			print("")
				#print("BEFORE: allPR list"+str(allPR.copy()))
				#print("BEFORE:PageRankList List:"+str(PageRanklist.copy()))
			for k, v in self.graph_dict.items():#looping to find PR with damping for node A
				if nodeA in v :
					outgoinglinks = len(v)
					x = PageRanklist[3]/AllOutgoingLinks[3]+PageRanklist[4]/AllOutgoingLinks[4]+PageRanklist[5]/	AllOutgoingLinks[5]
					a.append(x)
					PageRanklist.append(a[-1])
					#print("-----LET ME SEE WHAT IS PR LIST----"+str(PageRanklist.copy()))
					#df = (1-DampingFactor)+(DampingFactor)*(float(a[-1]))
					#PRWithDamping_List1.append(df)
					#PRWithDamping_List2.append(PRWithDamping_List1[-1])
				#print("A copy of the list"+str(pageranklist.copy()))
			allPR.append(PageRanklist[-1])
			del a[:]
			a.append(allPR[-1])
			del PageRanklist[6:9]
			PageRanklist[0]=a[0]
			df = (1-DampingFactor)+(DampingFactor)*(float(a[-1]))
			PRWithDamping_List1.append(df)
			PRWithDamping_List2.append(PRWithDamping_List1[-1])
			print("The new PR With Damping for node  " + nodeA + " is " +str(PRWithDamping_List2[-1]))#To print the last value of the list

				
				
			for k, v in self.graph_dict.items(): #looping to find PR with damping for node B
				if nodeB in v :
					outgoinglinks = len(v)
					x = PageRanklist[0]/AllOutgoingLinks[0]+PageRanklist[5]/AllOutgoingLinks[5]
					b.append(x)
					PageRanklist.append(b[-1])
					#df = (1-DampingFactor)+(DampingFactor)*(float(b[-1]))
					#PRWithDamping_List1.append(df)
					#PRWithDamping_List2.append(PRWithDamping_List1[-1])

				#print("A copy of the list"+str(pageranklist.copy()))
				#print("The new PR for node  " + nodeB + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del b[:]
			b.append(allPR[-1])
			del PageRanklist[6:9]
			PageRanklist[1]=b[0]
			df = (1-DampingFactor)+(DampingFactor)*(float(b[-1]))
			PRWithDamping_List1.append(df)
			PRWithDamping_List2.append(PRWithDamping_List1[-1])
			print("The new PR With Damping for node  " + nodeB + " is " +str(PRWithDamping_List2[-1]))

				
			for k, v in self.graph_dict.items():#looping to find PR with damping for node C
				if nodeC in v :
					outgoinglinks = len(v)
					x = PageRanklist[0]/AllOutgoingLinks[0]+PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[3]/AllOutgoingLinks[3]
					c.append(x)
					PageRanklist.append(c[-1])
					#df = (1-DampingFactor)+(DampingFactor)*(float(c[-1]))
					#PRWithDamping_List1.append(df)
				#PRWithDamping_List2.append(PRWithDamping_List1[-1])

			#print("A copy of the list"+str(pageranklist.copy()))
			#print("The new PR for node  " + nodeC + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del c[:]
			c.append(allPR[-1])
			del PageRanklist[6:9]
			PageRanklist[2]=c[0]
			df = (1-DampingFactor)+(DampingFactor)*(float(c[-1]))
			PRWithDamping_List1.append(df)
			PRWithDamping_List2.append(PRWithDamping_List1[-1])
			print("The new PR With Damping for node  " + nodeC + " is " +str(PRWithDamping_List2[-1]))


			for k, v in self.graph_dict.items():#looping to find PR with damping for node D
				if nodeD in v :
					outgoinglinks = len(v)
					x = PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[2]/AllOutgoingLinks[2]
					d.append(x)
					PageRanklist.append(d[-1])
					#df = (1-DampingFactor)+(DampingFactor)*(float(d[-1]))
					#PRWithDamping_List1.append(df)
					#PRWithDamping_List2.append(PRWithDamping_List1[-1])

			#print("A copy of the list"+str(pageranklist.copy()))
			#print("The new PR for node  " + nodeD + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del d[:]
			d.append(allPR[-1])
			del PageRanklist[6:9]
			PageRanklist[3]=d[0]
			df = (1-DampingFactor)+(DampingFactor)*(float(d[-1]))
			PRWithDamping_List1.append(df)
			PRWithDamping_List2.append(PRWithDamping_List1[-1])
			print("The new PR With Damping for node  " + nodeD + " is " +str(PRWithDamping_List2[-1]))


			for k, v in self.graph_dict.items():#looping to find PR with damping for node E
				if nodeE in v :
					outgoinglinks = len(v)
					x = PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[2]/AllOutgoingLinks[2]+PageRanklist[3]/AllOutgoingLinks[3]+PageRanklist[5]/AllOutgoingLinks[5]
					e.append(x)
					PageRanklist.append(e[-1])
						#df = (1-DampingFactor)+(DampingFactor)*(float(e[-1]))
						#PRWithDamping_List1.append(df)
						#PRWithDamping_List2.append(PRWithDamping_List1[-1])

			#print("A copy of the list"+str(pageranklist.copy()))
			#print("The new PR for node  " + nodeE + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del e[:]
			e.append(allPR[-1])
			del PageRanklist[6:10]
			PageRanklist[4]=e[0]
			df = (1-DampingFactor)+(DampingFactor)*(float(e[-1]))
			PRWithDamping_List1.append(df)
			PRWithDamping_List2.append(PRWithDamping_List1[-1])
			print("The new PR With Damping for node  " + nodeE + " is " +str(PRWithDamping_List2[-1]))

			
			
			
			
			for k, v in self.graph_dict.items():#looping to find PR with damping for node F
				if nodeF in v :
					outgoinglinks = len(v)
					x = PageRanklist[0]/AllOutgoingLinks[0]+PageRanklist[1]/AllOutgoingLinks[1]+PageRanklist[3]/AllOutgoingLinks[3]
					f.append(x)
					PageRanklist.append(f[-1])
					#df = (1-DampingFactor)+(DampingFactor)*(float(f[-1]))
					#PRWithDamping_List1.append(df)
					#PRWithDamping_List2.append(PRWithDamping_List1[-1])

			#print("A copy of the list"+str(pageranklist.copy()))
			#print("The new PR for node  " + nodeF + " is " +str(PageRanklist[-1]))
			allPR.append(PageRanklist[-1])
			del f[:]
			f.append(allPR[-1])
			del PageRanklist[6:10]
			PageRanklist[5]=f[0]
			df = (1-DampingFactor)+(DampingFactor)*(float(f[-1]))
			PRWithDamping_List1.append(df)
			PRWithDamping_List2.append(PRWithDamping_List1[-1])
			print("The new PR With Damping for node  " + nodeF + " is " +str(PRWithDamping_List2[-1]))

			print("")


			del PRWithDamping_List2[0:6]# Cleaning the final list from unncessary values

			NumberOfIterations = NumberOfIterations-1 #Decrement the loop





if __name__ == "__main__":# The main here to create the graph as dictionary and to apply the methods on the graph above by calling those methos
	
	g =  { "A" : ["B","C","F"],
		"B" : ["C", "D","E","F"],
		"C" : ["D", "E"],
		"D" : ["A", "C","E","F"],
		"E" : ["A"],
		"F" : ["A", "B","E"]
		}
	
	graph = Graph(g)
	print("")
	print("Vertices of the graph:")
	print("")
	graph.vertices()
	print("")
	print("Edges of the graph:")
	graph.edges()
	print("")
	node=input("Enter the node name to check if it is related to the graph: ")
	print("")
	graph.finder(node)
	graph.counting_keys()
	graph.get_nodes_outgoinglinks()
	graph.counting_values()
	#node=input("Enter the node name: ")
	#graph.get_nodes_existence(node)
	node=input("Enter the node name to find the pagerank: ")
	graph.individual_pagerank_calculation(node)
	nodeA,nodeB,nodeC,nodeD,nodeE,nodeF=input("Enter nodes to find the pagerank: ").split()
	graph.pageranks_calculation(nodeA,nodeB,nodeC,nodeD,nodeE,nodeF)
	nodeA,nodeB,nodeC,nodeD,nodeE,nodeF=input("Enter nodes to find the pagerank: ").split()
	graph.pagerank_withDamping(nodeA,nodeB,nodeC,nodeD,nodeE,nodeF)










