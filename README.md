# COMP460-Project- PageRank
#Summary:
	In this project, we created a code to apply the pagerank on the following graph (Just like the one we had on the exam). We created the graph in python and by using dictionary. Where each node represnt the keys of the dictionary and the values of each node is the outgoing links from that key.
	You can find the picture of the graph in this repo. In this code, we are able to find the followings:
	1. Find the vertices of the graph by this method: graph.vertices()
	2. Find the edges of the grapg by this method: graph.edges()
	3. Check if a node is in this graph or not by this method: graph. finder(node)
	4. Count how many nodes in the graph by this method: graph.counting_keys()
	5. Count how many outgoing links from each node by this method: graph.get_nodes_outgoinglinks()
	6. Count the total of outgoing links for each node by this method: graph.counting_values()
	7. Find the PageRank for a node by this method: graph.individual_pagerank_calculation(node)
	8. Find the PR for all nodes without damping facotr by:graph.pageranks_calculation(nodeA,nodeB,nodeC,nodeD,nodeE,nodeF)
	9.  Find the PR for all nodes with damping facotr by: graph.pagerank_withDamping(nodeA,nodeB,nodeC,nodeD,nodeE,nodeF)
	How to run this code(For MAC users):
	- By Termianl go to the folder that has pagerank.py
	- Type python pagerank.py then follow the instructions that will show up to lead you to find the page rank.
	
	
	Online Refrences: 
	https://www.python-course.eu/graphs_python.php (How to create a graph)
	https://developer.rhino3d.com/guides/rhinopython/python-dictionaries/  (Python Dictionaries)
	https://thispointer.com/python-how-to-find-keys-by-value-in-dictionary/ (Keys and values in Dictionaries)
	https://www.w3schools.com/python/
	https://stackoverflow.com/questions/5735841/typeerror-list-object-is-not-callable-while-trying-to-access-a-list 
	https://stackoverflow.com/questions/8120019/typeerror-float-object-not-iterable 
	
	
	
	
	
	
